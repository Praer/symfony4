<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TestEntity;
use App\Services\Api\Adapter;
use Psr\SimpleCache\CacheInterface;


class MainController extends AbstractController
{

    /**
     * key cache
     */
    const KEY = 'api';

    /**
     * cache lifetime
     */
    const TIME = 9999;

    /**
     * @Route("/", name="main")
     *
     * @param Adapter $api
     * @param CacheInterface $cache
     * @return void
     */
    public function index(Adapter $api, CacheInterface $cache)
    {
        try {
            if (!$cache->has(self::KEY)) {
                $cache->set(self::KEY, $api->getData(), self::TIME);
            }

            return $this->json(
                 $cache->get(self::KEY)
            );

        } catch (\Exception $e) {
            return $this->json([
                'message' => 'sorry! ' . $e->getMessage(),
            ]);
        }
    }

    /**
     * @Route("/test", name="main")
     */
    public function testEntity()
    {
        try {
            $repository = $this->getDoctrine()->getRepository(TestEntity::class);

            $test = $repository->finRandomField();

            if (!$test) {
                throw $this->createNotFoundException(
                    'No test entity found'
                );
            }

            return $this->json([
                'id' => $test->getId(),
                'title' => $test->getTitle(),
                'weight' => $test->getWeight(),
            ]);

        } catch (\Exception $e) {
            return $this->json([
                'message' => 'sorry! ' . $e->getMessage(),
            ]);
        }
    }
}
