<?php 

namespace App\Services\Api;

use App\Services\Api\InterfaceSanitizer;

class Sanitizer implements InterfaceSanitizer
{
    public function sanitize($data) : array
    {
        $sanitizeData = [
            'id' => $data['id'],
            'title' => $data['title'],
        ];

        return $sanitizeData;
    }
}